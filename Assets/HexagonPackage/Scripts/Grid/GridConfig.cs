﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HexagonPackage
{
	[CreateAssetMenu]
    public class GridConfig : ScriptableObject
	{
        public int XSpacing;
        public int YSpacing;
	}
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace HexagonPackage
{	
    public class HexGrid : MonoBehaviour
	{
        public float XSpacing = 10f;
        public float YSpacing = 10f;

        [SerializeField] private GameObject hexagonPrefab = null;

        public Dictionary<Cube, Hexagon> ActiveHexagons = new Dictionary<Cube, Hexagon>();

        private List<Hexagon> hexagonObjects = new List<Hexagon>();

        public SavedGrid GridToLoad;

        public event EventHandler<Hexagon> HexagonCreated;
        public event EventHandler<SavedGrid> GridLoaded;

        private void OnValidate()
        {
            GetChildrenHexagons();
        }
        private void Awake()
        {
            //GetChildrenHexagons();
        }

        private void GetChildrenHexagons()
        {
            ActiveHexagons = new Dictionary<Cube, Hexagon>();
            hexagonObjects = new List<Hexagon>();
            for (int i = 0; i < transform.childCount; i++)
            {
                if (transform.GetChild(i).gameObject.activeSelf == false)
                {
                    continue;
                }
                Hexagon hex = transform.GetChild(i).GetComponent<Hexagon>();
                if (hex != null)
                {
                    ActiveHexagons.Add(hex.Cube, hex);
                    hexagonObjects.Add(hex);
                }
            }
        }
        public void RemoveAll()
        {
            foreach (var hex in ActiveHexagons.Values)
            {
                hex.gameObject.SetActive(false);
            }
            for (int i = 0; i < ActiveHexagons.Count; i++)
            {
                ActiveHexagons.ToLookup(x => x.Value.gameObject);
                ActiveHexagons.ElementAt(i).Value.gameObject.SetActive(false);
            }
            ActiveHexagons = new Dictionary<Cube, Hexagon>();
        }
        public void RemoveHexagon(Cube cube)
        {
            Hexagon hex;
            ActiveHexagons.TryGetValue(cube, out hex);
            if (hex == null)
            {
                Debug.LogWarning("Hex with pos " + cube + " doesn't exist");
                return;
            }
            RemoveHexagon(hex);
        }
        public void RemoveHexagon(Hexagon hexagon)
        {
            if (hexagon == null)
            {
                Debug.LogWarning("Hexagon to be removed is null");
                return;
            }
            ActiveHexagons.Remove(hexagon.Cube);
            hexagon.gameObject.SetActive(false);
        }

        public void LoadGrid(SavedGrid savedGrid)
        {
            for (int i = transform.childCount - 1; i >= 0; i--)
            {
                DestroyImmediate(transform.GetChild(i).gameObject);
            }
            //for (int i = 0; i < transform.childCount; i++)
            //{
            //    transform.GetChild(i).gameObject.SetActive(false);
            //}
            ActiveHexagons = new Dictionary<Cube, Hexagon>();
            foreach (var hexPositions in savedGrid.SavedHexagonPositions)
            {
                foreach (var gridPos in hexPositions.SavedPositions)
                {
                    CreateHexagon(gridPos).HexagonType = hexPositions.HexagonType;
                }              
            }
            GridLoaded?.Invoke(this, savedGrid);
        }
        public Hexagon CreateHexagon(Cube pos)
        {
            if (ActiveHexagons.ContainsKey(pos))
            {
                Debug.LogWarning("Hex with pos " + pos + " already exists");
                return null;
            }
            Hexagon hex = GetEmptyHexagon();
            hex.Setup(pos, this);            
            return hex;
        }
        private Hexagon GetEmptyHexagon()
        {
            if (hexagonObjects.Any(x => x == null))
            {
                hexagonObjects.Clear();
            }
            if (hexagonObjects.Any(x => x.gameObject.activeSelf == false))
            {
               return hexagonObjects.Find(x => x.gameObject.activeSelf == false);
            }
            else
            {
                GameObject hexGO = Instantiate(hexagonPrefab, transform);
                Hexagon hex = hexGO.GetComponent<Hexagon>();
                HexagonCreated?.Invoke(this, hex);
                hexagonObjects.Add(hex);
                return hex;
            } 
        }
        public List<Hexagon> GetNeighbours(Cube cube)
        {
            List<Cube> neighbours = cube.GetNeighbours();
            List<Hexagon> validHexes = ValidateCubes(neighbours);
            return validHexes;
        }
        public List<Hexagon> GetNeighbours(Hexagon hex)
        {
            return GetNeighbours(hex.Cube);
        }

        private List<Hexagon> ValidateCubes(List<Cube> cubes)
        {
            List<Hexagon> hexagons = new List<Hexagon>();
            foreach (var cube in cubes)
            {
                Hexagon hex;
                ActiveHexagons.TryGetValue(cube, out hex);
                if (hex != null)
                {
                    hexagons.Add(hex);
                }
            }
            return hexagons;
        }
        public bool ValidateCube(Cube cube)
        {
            if (ActiveHexagons.ContainsKey(cube))
            {                
                return true;
            }
            return false;
        }
	}
}
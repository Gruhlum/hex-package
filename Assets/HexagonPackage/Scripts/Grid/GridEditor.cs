﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace HexagonPackage
{
    public class GridEditor : MonoBehaviour
    {
        public HexGrid ActiveGrid;

        public HexagonTypeCollection AvailableHexTypes;
        public HexagonType EditType;

        public HexagonType SelectedType;

        private bool enableHoverClick = false;
        private int lastMouseButton;

        private void Awake()
        {           
            foreach (var hex in ActiveGrid.ActiveHexagons.Values)
            {
                SetupHexagonClickEvents(hex);
            }
            ActiveGrid.HexagonCreated += OnHexagonCreated;
            ActiveGrid.GridLoaded += ActiveGrid_GridLoaded;
            CreateEditGrid();
        }

        private void ActiveGrid_GridLoaded(object sender, SavedGrid e)
        {
            CreateEditGrid();
        }
        
        private void Update()
        {
            if (Input.GetMouseButtonUp(lastMouseButton))
            {
                enableHoverClick = false;
            }
        }
        
        private void SetupHexagonClickEvents(Hexagon hex)
        {
            ClickEvents clickEvents = hex.GetComponentInChildren<ClickEvents>();
            if (clickEvents != null)
            {
                clickEvents.HexagonLeftClicked += OnHexagonLeft_Clicked;
                clickEvents.HexagonRightClicked += OnHexagonRight_Clicked;
                clickEvents.HexagonMouseEnter += OnHexagonMouse_Enter;
            }
        }
        private void OnHexagonCreated(object sender, Hexagon hex)
        {
            SetupHexagonClickEvents(hex);
        }
        private void OnHexagonLeft_Clicked(object sender, Hexagon hex)
        {
            lastMouseButton = 0;
            enableHoverClick = true;
            TransformHex(hex);
        }

        private void OnHexagonRight_Clicked(object sender, Hexagon hex)
        {
            lastMouseButton = 1;
            enableHoverClick = true;
            RemoveHexagon(hex);
        }
        private void OnHexagonMouse_Enter(object sender, Hexagon hex)
        {
            if (enableHoverClick)
            {
                if (lastMouseButton == 0)
                {
                    TransformHex(hex);
                }
                else if (lastMouseButton == 1)
                {
                    RemoveHexagon(hex);
                }
            }
        }
        private void TransformHex(Hexagon hex)
        {
            if (SelectedType != null)
            {
                if (hex.HexagonType == EditType)
                {
                    CreateEditNeighbours(hex);
                }
                hex.HexagonType = SelectedType;
            }
        }
        private void CreateEditNeighbours(Hexagon createdHex)
        {
            List<Cube> neighbours = createdHex.Cube.GetNeighbours();
            foreach (var neighbour in neighbours)
            {
                if (ActiveGrid.ActiveHexagons.ContainsKey(neighbour) == false)
                {
                    ActiveGrid.CreateHexagon(neighbour).HexagonType = EditType;
                }
            }           
        }
        private void RemoveHexagon(Hexagon hex)
        {            
            if (hex.HexagonType == EditType)
            {
                return;
            }           

            if (ActiveGrid.GetNeighbours(hex.Cube).Any(x => x.HexagonType != EditType))
            {
                hex.HexagonType = EditType;               
            }
            else ActiveGrid.RemoveHexagon(hex);

            RemoveEditNeighbours(hex);
        }
        private void RemoveEditNeighbours(Hexagon hex)
        {
            List<Cube> neighbours = hex.Cube.GetNeighbours();
            List<Cube> editHexesToRemove = new List<Cube>();

            foreach (var neighbour in neighbours)
            {
                if (ActiveGrid.ActiveHexagons[neighbour].HexagonType == EditType)
                {
                    if (ActiveGrid.GetNeighbours(neighbour).Any(x => x.HexagonType != EditType) == false)
                    {
                        editHexesToRemove.Add(neighbour);
                    }
                }               
            }
            for (int i = editHexesToRemove.Count - 1; i >= 0; i--)
            {
                ActiveGrid.RemoveHexagon(editHexesToRemove[i]);
            }                   
        }

        private List<Cube> GetMissingNeighbours()
        {
            List<Cube> emptyNeighbours = new List<Cube>();
            foreach (var hex in ActiveGrid.ActiveHexagons.Values)
            {
                List<Cube> neighbours = hex.Cube.GetNeighbours();
                foreach (var neighbour in neighbours)
                {
                    // Is it already in the list or does a hex already exist at that position?
                    if (!emptyNeighbours.Any(x => x == neighbour) && ActiveGrid.ValidateCube(neighbour) == false)
                    {
                        emptyNeighbours.Add(neighbour);
                    }                   
                }
            }
            return emptyNeighbours;
        }
        private void CreateEditGrid()
        {
            List<Cube> missingNeighbours = GetMissingNeighbours();
            foreach (var cube in missingNeighbours)
            {
                ActiveGrid.CreateHexagon(cube).HexagonType = EditType;
            }           
        }
    }
}

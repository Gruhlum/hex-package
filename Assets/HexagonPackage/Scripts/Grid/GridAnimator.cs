﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace HexagonPackage
{
	[RequireComponent(typeof(HexGrid))]
    public class GridAnimator : MonoBehaviour
	{
        private HexGrid hexGrid;

        private int degreesLeft;

        [Range(1, 30)]
        public int speed;

        private int rotation = 0;

        public event EventHandler<EventArgs> GridRotationFinished;


        private void Awake()
        {
            hexGrid = GetComponent<HexGrid>();
        }
        private void FixedUpdate()
        {
            if (degreesLeft == 0)
            {
                return;
            }

            if (degreesLeft > 0)
            {
                transform.eulerAngles += new Vector3(0, 0, 2 * speed);
                degreesLeft -= 2 * speed;
                if (degreesLeft == 0)
                {
                    foreach (var hex in hexGrid.ActiveHexagons.Values)
                    {
                        hex.Cube.Rotate(new Cube(0, 0), 1 * Mathf.Abs(rotation));
                    }                   
                }
            }
            else if (degreesLeft < 0)
            {
                transform.eulerAngles -= new Vector3(0, 0, 2 * speed);
                degreesLeft += 2 * speed;
                if (degreesLeft == 0)
                {
                    foreach (var hex in hexGrid.ActiveHexagons.Values)
                    {
                        hex.Cube.Rotate(new Cube(0, 0), 5 * Mathf.Abs(rotation));
                    }
                }
            }
            rotation = 0;
            transform.localPosition += new Vector3(0, 0, 1);
            GridRotationFinished?.Invoke(this, new EventArgs());
        }

        public void SetRotation(int steps)
        {
            transform.localPosition -= new Vector3(0, 0, 1);
            degreesLeft += steps * 60;
            rotation += steps;
        }

    }
}
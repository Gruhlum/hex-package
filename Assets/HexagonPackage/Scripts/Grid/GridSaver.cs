﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace HexagonPackage
{
    [RequireComponent(typeof(GridEditor))]
    public class GridSaver : MonoBehaviour
    {
        public string folderLocation = "Assets/HexagonPackage/Scripts/SavedGrids/Grids";
        public Transform HexagonParent;
        public string GridName;

        private bool overrideSave;

        [SerializeField] private GridEditor gridEditor = null;

        private void Awake()
        {
            Application.quitting += Application_quitting;
        }
        private void Reset()
        {
            gridEditor = GetComponent<GridEditor>();
        }

        private void Application_quitting()
        {
            SavePositions("Unsaved", true);
        }

        private bool AssetAlreadyExist(string name)
        {
#if (UNITY_EDITOR)
            string[] results = AssetDatabase.FindAssets(GridName, new string[] { folderLocation });
            foreach (var result in results)
            {
                if (result == name)
                {
                    return true;
                }
            }
#endif
            return false;
        }

        public void SavePositions(string name = "", bool force = false)
        {
#if (UNITY_EDITOR)
            if (name == "")
            {
                if (GridName == "")
                {
                    Debug.LogError("No Name");
                    return;
                }
                name = GridName;
            }
            
            if (force == false && overrideSave == false && AssetAlreadyExist(name))
            {
                Debug.Log(name + " already exists. Click again to override.");
                overrideSave = true;
                return;
            }
            overrideSave = false;

            SavedGrid savedGrid = ScriptableObject.CreateInstance<SavedGrid>();
            savedGrid.SaveGridData(gridEditor.ActiveGrid.ActiveHexagons.Values, gridEditor.EditType);

            AssetDatabase.CreateAsset(savedGrid, folderLocation + "/" + name + ".asset");

            Debug.Log("Saved Successful");
            GridName = "";
#endif
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HexagonPackage
{
    [CreateAssetMenu]
    public class HexagonType : ScriptableObject
	{
        public Color Color = Color.white;
	}
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace HexagonPackage
{
    [RequireComponent(typeof(Collider2D))]
    public class ClickEvents : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler
    {
        private Hexagon hexagon;

        public event EventHandler<Hexagon> HexagonLeftClicked;
        public event EventHandler<Hexagon> HexagonRightClicked;
        public event EventHandler<Hexagon> HexagonMouseEnter;

        private void Awake()
        {
            hexagon = GetComponentInParent<Hexagon>();
        }
        public void OnPointerDown(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Left)
            {
                HexagonLeftClicked?.Invoke(this, hexagon);
            }
            if (eventData.button == PointerEventData.InputButton.Right)
            {
                HexagonRightClicked?.Invoke(this, hexagon);
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            HexagonMouseEnter?.Invoke(this, hexagon);
        }
    }
}

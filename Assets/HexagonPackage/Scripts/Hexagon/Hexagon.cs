﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace HexagonPackage
{
    [ExecuteInEditMode]
    public class Hexagon : MonoBehaviour
    {
        public static readonly float WIDTH_MULTIPLIER = 0.866025f; //Mathf.Sqrt(3) / 2;        

        public float Radius
        {
            get
            {
                return radius;
            }
        }
        private float radius = 150;
        public float HexHeight
        {
            get
            {
                return Radius * 2;
            }
        }
        public float HexWidth
        {
            get
            {
                return WIDTH_MULTIPLIER * HexHeight;
            }
        }
        public float HexVerticalSpacing
        {
            get
            {
                return HexHeight * 0.75f + xSpacing;
            }
        }
        public float HexHorizontalSpacing
        {
            get
            {
                return HexWidth + ySpacing;
            }
        }
        
        public Cube Cube
        {
            get
            {
                return cube;
            }
            protected set
            {
                cube = value;
            }
        }
        [SerializeField] private Cube cube;        

        public HexagonType HexagonType
        {
            get
            {
                return hexagonType;
            }
            set
            {
                hexagonType = value;
                GetComponent<SpriteRenderer>().color = value.Color;
            }
        }
        [SerializeField] private HexagonType hexagonType;

        private float xSpacing;
        private float ySpacing;

        private HexGrid Grid;

        public virtual void Setup(Cube cube, HexGrid grid)
        {
            Grid = grid;
            Cube = cube;
            xSpacing = grid.XSpacing;
            ySpacing = grid.YSpacing;
            transform.localPosition = ToWorldPosition(cube.X, cube.Y);
            name = "Hex (" + Cube.X + ", " + Cube.Y + ")";
            if (!grid.ActiveHexagons.ContainsKey(Cube))
            {
                Grid.ActiveHexagons.Add(Cube, this);
            }
            gameObject.SetActive(true);
        }

        public Vector2 ToWorldPosition(int x, int y)
        {
            return new Vector2(HexHorizontalSpacing * (x + y / 2f), HexVerticalSpacing * y);
        }

        public virtual void Highlight(bool value)
        {
            //if (value)
            //{
            //    GetComponent<SpriteRenderer>().color = HexagonType.Color + new Color(0.2f, 0.2f, 0.2f);
            //}
            //else
            //{
            //    GetComponent<SpriteRenderer>().color = HexagonType.Color;
            //}
        }
    }
}
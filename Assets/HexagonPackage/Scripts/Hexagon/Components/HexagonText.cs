﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace HexagonPackage
{
    [RequireComponent(typeof(Hexagon))]
    public class HexagonText : MonoBehaviour
	{
        [SerializeField] private GameObject textPrefab = null;

        private Text hexText;
        private void Awake()
        {
            CreateTextCanvas();
        }
        private void FixedUpdate()
        {
            if (hexText == null)
            {
                return;
            }
            hexText.transform.eulerAngles = new Vector3(0, 0, 0);
        }
        private void CreateTextCanvas()
        {           
            GameObject canvasGO = Instantiate(textPrefab, transform);           
            hexText = canvasGO.GetComponent<Text>();
        }

        public void SetText(string text)
        {
            hexText.text = text;
        }
        public string GetText()
        {
            return hexText.text;
        }
    }
}
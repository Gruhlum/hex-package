﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace HexagonPackage
{
    [System.Serializable]
    public class Cube
    {
        public int X;
        public int Y;
        public int Z;

        //public Cube cameFrom;

        public static readonly List<Cube> cubeDirections = new List<Cube>
            { new Cube(1, -1), new Cube(1, 0), new Cube(0, 1),
            new Cube(-1, 1), new Cube(-1, 0), new Cube(0, -1)};


        public Cube(int x, int y)
        {
            X = x;
            Y = y;
            Z = -(x + y);
        }
        public Cube(int x, int z, int nothing)
        {
            X = x;
            Y = -(x + z);
            Z = z;
        }

        public void SetValues(Cube cube)
        {
            X = cube.X;
            Y = cube.Y;
            Z = cube.Z;
        }
        public void SetValues(int x, int y)
        {
            X = x;
            Y = y;
            Z = -(x + y);
        }

        public void Add(Cube cube)
        {
            SetValues(X + cube.X, Y + cube.Y);
        }
        public void Subtract(Cube cube)
        {
            SetValues(X - cube.X, Y - cube.Y);
        }
        public void Multiply(int value)
        {
            SetValues(X * value, Y * value);
        }

        public Cube GetNeighbour(int direction)
        {
            Cube cube = new Cube(X, Y);
            cube.Add(cubeDirections[direction]);
            return cube;
        }
        public List<Cube> GetNeighbours()
        {
            List<Cube> Results = new List<Cube>();
            foreach (var direction in cubeDirections)
            {
                Cube cube = new Cube(X, Y);
                cube.Add(direction);
                Results.Add(cube);
            }
            return Results;
        }
        public bool IsNeighbour(Cube cube)
        {
            if (GetNeighbours().Any(x => x == cube))
            {
                return true;
            }
            return false;
        }

        public void Rotate(Cube center, int direction)
        {
            Subtract(center);
            for (int i = 0; i < direction; i++)
            {
                SetValues(-Y, -Z);
            }
            Add(center);
        }

        //public bool Compare(List<Cube> cubes)
        //{
        //    foreach (var cube in cubes)
        //    {
        //        if (this.Compare(cube))
        //        {
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        public List<Cube> GetPath(Cube cube)
        {
            List<Cube> pathCubes = new List<Cube>();

            if (this == cube)
            {
                pathCubes.Add(this);
                return pathCubes;
            }

            int distance = GetDistance(cube);

            for (int i = 0; i <= distance; i++)
            {
                float t = ((float)i / (float)distance);
                pathCubes.Add(Lerp(this, cube, t));
            }
            return pathCubes;
        }

        public int? GetDirection(Cube target)
        {
            if (target == null || this == target)
            {
                return null;
            }

            List<Cube> path = GetPath(target);
            if (path.Count <= 1)
            {
                return null;
            }

            Cube firstCube = path.ElementAt(1);
            firstCube.Subtract(target);
            return cubeDirections.IndexOf(firstCube);
        }

        public int GetDistance(Cube cube)
        {
            return (Mathf.Abs(X - cube.X) + Mathf.Abs(Y - cube.Y) + Mathf.Abs(Z - cube.Z)) / 2;
        }

        private static Cube Lerp(Cube c1, Cube c2, float t)
        {
            float x = Mathf.Lerp(c1.X, c2.X, t);
            float y = Mathf.Lerp(c1.Y, c2.Y, t);
            float z = Mathf.Lerp(c1.Z, c2.Z, t);

            return Round(x, y, z);
        }
        private static Cube Round(float x, float y, float z)
        {
            int rX = Mathf.RoundToInt(x - 0.1f);
            int rY = Mathf.RoundToInt(y);
            int rZ = Mathf.RoundToInt(z);

            float xDiff = Mathf.Abs(rX - x);
            float yDiff = Mathf.Abs(rY - y);
            float zDiff = Mathf.Abs(rZ - z);

            if (xDiff > yDiff && xDiff > zDiff)
            {
                rX = -rY - rZ;
            }
            else if (yDiff > zDiff)
            {
                rY = -rX - rZ;
            }
            else rZ = -rX - rY;
            return new Cube(rX, rY);
        }

        public static Cube GetCubeFromDirection(int direction)
        {
            if (direction < 0)
            {
                direction += 6;
            }
            if (direction > 5)
            {
                direction -= 6;
            }
            return cubeDirections[direction];
        }


        private bool Compare(Cube cube)
        {
            if (X == cube.X && Y == cube.Y)
            {
                return true;
            }
            else return false;
        }

        public static bool operator ==(Cube cube1, Cube cube2)
        {
            if (cube1 is null || cube2 is null)
            {
                if (cube1 is null && cube2 is null)
                {
                    return true;
                }
                return false;
            }
            return cube1.Compare(cube2);
        }

        public static bool operator !=(Cube cube1, Cube cube2)
        {
            return !(cube1 == cube2);
        }

        public override string ToString()
        {
            return "Cube (" + X + ", " + Y + ")";
        }

        public override bool Equals(object obj)
        {
            if (obj is Cube cube)
            {
                if (this.X == cube.X && this.Y == cube.Y)
                {
                    return true;
                }
            }
            return false;
        }

        public override int GetHashCode()
        {
            if (this == null) return 0;
            return X.GetHashCode() + Y.GetHashCode();
        }

    }
}
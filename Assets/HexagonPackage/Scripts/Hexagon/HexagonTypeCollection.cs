﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HexagonPackage
{
    public class HexagonTypeCollection : ScriptableObject
	{
        public List<HexagonType> HexagonTypes;
	}
}
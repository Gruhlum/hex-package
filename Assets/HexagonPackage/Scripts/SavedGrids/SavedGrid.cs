﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace HexagonPackage
{
    public class SavedGrid : ScriptableObject
    {
        public List<HexagonPositions> SavedHexagonPositions = new List<HexagonPositions>();

        public void SaveGridData(Dictionary<Cube, Hexagon>.ValueCollection hexagons, HexagonType ignoreType)
        {
            SavedHexagonPositions = new List<HexagonPositions>();

            foreach (var hex in hexagons)
            {
                if (SavedHexagonPositions.Any(x => x.HexagonType == hex.HexagonType))
                {
                    SavedHexagonPositions.Find(x => x.HexagonType == hex.HexagonType).SavedPositions.Add(hex.Cube);
                }
                else if (hex.HexagonType != ignoreType)
                {
                    HexagonPositions hexPositions = new HexagonPositions
                    {
                        HexagonType = hex.HexagonType,
                        SavedPositions = new List<Cube>() { hex.Cube },
                    };
                    SavedHexagonPositions.Add(hexPositions);
                }
            }           
        }
        [System.Serializable]
        public struct HexagonPositions
        {
            public HexagonType HexagonType;
            public List<Cube> SavedPositions;
        }
    }
}

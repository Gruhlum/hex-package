﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace HexagonPackage
{
	[CustomEditor(typeof(HexGrid))]
	public class HexGridEditor : Editor
	{
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            HexGrid hexGrid = (HexGrid)target;
            if (!Application.isPlaying && hexGrid.GridToLoad != null)
            {
                if (GUILayout.Button("Load Grid"))
                {
                    hexGrid.LoadGrid(hexGrid.GridToLoad);
                }
            }           
        }
    }
}
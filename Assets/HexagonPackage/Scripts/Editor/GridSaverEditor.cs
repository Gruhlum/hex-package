﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace HexagonPackage
{
    [CustomEditor(typeof(GridSaver))]
    public class GridSaverEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            GridSaver gridSaver = (GridSaver)target;

            if (GUILayout.Button("Save"))
            {
                gridSaver.SavePositions();
            }
        }
    }
}

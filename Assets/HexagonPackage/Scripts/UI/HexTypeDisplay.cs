﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace HexagonPackage
{
	public class HexTypeDisplay : MonoBehaviour
	{
        [SerializeField] private Image hexImage = null;
        [SerializeField] private Image background = null;
        [SerializeField] private Text hotKeyText = null;

        public Color SelectedColor;
        public Color DefaultColor;

        public HexagonType HexType;

        public HotKey HotKey;

        public void Setup(HexagonType hexType, HexTypeManager manager, HotKey hotKey)
        {
            hexImage.color = hexType.Color;
            HexType = hexType;
            HotKey = hotKey;
            this.hotKeyText.text = hotKey.DisplayText;
            GetComponent<Button>().onClick.AddListener(delegate { manager.OnButtonClicked(this); });
        }
        public void ToggleBackground(bool active)
        {
            if (active)
            {
                background.color = SelectedColor;
            }
            else background.color = DefaultColor;
        }
	}
}
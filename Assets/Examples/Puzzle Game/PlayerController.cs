﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HexagonPackage
{
	public class PlayerController : MonoBehaviour
	{
        private GameController gameController;

        private void Awake()
        {
            gameController = FindObjectOfType<GameController>();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                gameController.SelectedRing++;
            }
            if (Input.GetKeyDown(KeyCode.S))
            {
                gameController.SelectedRing--;
            }
            if (Input.GetKeyDown(KeyCode.A))
            {
                gameController.RotateRing(1);
            }
            if (Input.GetKeyDown(KeyCode.D))
            {
                gameController.RotateRing(-1);
            }
            if (Input.GetKeyDown(KeyCode.T))
            {
                gameController.Swap();
            }
        }
    }
}
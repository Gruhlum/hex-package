﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HexagonPackage
{
	public class GameController : MonoBehaviour
	{
        public List<HexGrid> HexRings;

        public List<HexagonType> PossibleTypes;

        public int SelectedRing
        {
            get
            {
                return selectedRing;
            }
            set
            {
                if (value > HexRings.Count - 1)
                {
                    value = HexRings.Count - 1;
                }
                if (value <= 0)
                {
                    value = 1;
                }
                if (selectedRing == value)
                {
                    return;
                }
                //foreach (var hex in HexRings[SelectedRing].ActiveHexagons)
                //{
                //    hex.Highlight(false);
                //}

                //selectedRing = value;

                //foreach (var hex in HexRings[SelectedRing].ActiveHexagons)
                //{
                //    hex.Highlight(true);
                //}
            }
        }
        private int selectedRing = 0;
        internal void Swap()
        {
            //Hexagon hex = HexRings[1].ActiveHexagons.Find(x => x.Cube == new Cube(-1, 1));
            //int numb1 = GetNumber(hex);
            //HexagonType hexType = hex.HexagonType;
            //Hexagon hex2 = HexRings[2].ActiveHexagons.Find(x => x.Cube == new Cube(-2, 2));
            //int numb2 = GetNumber(hex2);
            //int numbResult = (int)(numb1 / 2f);
            //if (numb1 == numb2)
            //{
                //hex.SetText(numbResult.ToString());
                //hex2.SetText(numbResult.ToString());
            //}
            //hex.HexagonType = hex2.HexagonType;
            //hex2.HexagonType = hexType;

        }
        private int GetNumber(Hexagon hex)
        {
            //string text = hex.GetText();
            //int number = System.Convert.ToInt32(text);
            return 0;
        }

        private void Start()
        {
            AssignColors();
            SelectedRing = 1;
        }

        private void AssignColors()
        {
            foreach (var grid in HexRings)
            {
                foreach (var hex in grid.ActiveHexagons)
                {
                    //hex.HexagonType = GetRandomType();
                    //if (hex is AdvancedHex advHex)
                    //{
                    //    int random = Random.Range(1, 5);
                    //    if (Random.Range(0, 2) == 0)
                    //    {
                    //        //advHex.SetText((random).ToString());
                    //        advHex.HexagonType = PossibleTypes[2];
                    //    }
                    //    else
                    //    {
                    //        advHex.HexagonType = PossibleTypes[1];
                    //        //advHex.SetText((random).ToString());
                    //    } 
                    //}                   
                }
            }           
        }
        private int GetNumber()
        {
            int sum = 0;
            for (int i = 1; i < HexRings.Count; i++)
            {
                //sum += System.Convert.ToInt32(HexRings[i].ActiveHexagons.Find(x => x.Cube == new Cube(i * -1, 0)).GetText());
                //sum -= System.Convert.ToInt32(HexRings[i].ActiveHexagons.Find(x => x.Cube == new Cube(i, 0)).GetText());
            }
            return sum;
        }

        private HexagonType GetRandomType()
        {
            return PossibleTypes[Random.Range(0, PossibleTypes.Count)];
        }

        public void RotateRing(int degrees)
        {
            HexRings[SelectedRing].GetComponent<GridAnimator>().SetRotation(degrees);
        }
    }
}